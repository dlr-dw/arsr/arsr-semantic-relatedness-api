# Semantic Relatedness Module

Java implementation of the semantic relatedness service as part of the ARSR architecture.  

A [Jetty](https://www.eclipse.org/jetty/) server and [Jersey](https://eclipse-ee4j.github.io/jersey/) as JAX-RS implementation is used to provide REST endpoints.  

Additionally, the [LDS-Library](https://github.com/FouadKom/lds) is used for implementations of relatedness measures.  
It should be usable in its current state but some bugs were fixed in the version we use.  
For this reason, we modified it and added this adjusted version to [repository/io/github/FouadKom](repository/io/github/FouadKom) (a local Maven repository).  
Please note that this version and `LDS` in general uses the **AGPL-3.0 license** (see respective LICENSE file in the folder).  
Our source code is licensed as described by [LICENSE.txt](LICENSE.txt) in the repository root folder.

Java version `17.0.2 (LTS)` was used to create this project (using java 11 works as well).  
The application was tested on Windows and Linux (lastly: May 2022).



## Build using Maven

This project was created with the `Eclipse IDE` and uses `Apache Maven` for dependency management and building.  

If Java `17` is not available on your system, edit the file `pom.xml`.  
Under `properties` change the version in `maven.compiler.source` and `maven.compiler.target` accordingly.  
This was tested with Java `11` (used by the evaluation system) and works just fine.  

Compile the source code using:
```
mvn clean compile
```

You can also use `mvn verify` to run the validate, compile, package commands in this order.  

Use the [exec-maven-plugin](https://www.mojohaus.org/exec-maven-plugin/) to start the application:
```
mvn exec:java
```


#### Build Executable JAR

To build an executable JAR, run this command:
```
mvn clean package
```

Dependencies are copied to a folder called `libs`.  
This folder must be located in the same directory as the JAR file before execution.  

To run the executable JAR, you can use this command:
```
java -server -jar <filename.jar>
```


#### Execution Parameters

The following is an example for how to use these parameters.  
Note that they start with `-D` and must be set before the "`-jar ...`" part.  
```
java -server -Dport=80 -Dindexing=false -jar <filename.jar>
```

These are the currently available options:
- `host` : Hostname of the server (default: `http://localhost`)
- `port` : Server port (default: `8080`)
- `sparqlEndpoint` : Address of SPARQL endpoint (default: `http://localhost:9999/blazegraph/sparql`)
- `graph` : Name of the default graph (default: `""` (empty))
- `indexing` : Enable/disable indexing of LDS implementation (default: `true`)
	- If enabled, requests results will be stored in a file on disk.
	- The system is not perfect but can increase the performance of repeated requests a lot.



## Usage Example

#### Request

Request method: `POST`  
Content-Type: application/json  

The body contains `JSON` of following structure:  
```json
{
  "entities": [
    { "a": "firstURI1", "b": "secondURI1" },
    { "a": "firstURI2", "b": "secondURI2" }
  ]
}
```

The "entities" list can be of an arbitrary size.  

This request can be send to one of the implemented endpoints, e.g.:
- `http://localhost:8080/api/relatedness/ldsd`
- `http://localhost:8080/api/relatedness/resim`

Here is an example using `curl`:
```
curl -D- -X POST -H 'Content-Type: application/json' --data '{"entities":[{"a":"URI1","b":"URI2"}]}' 'http://localhost:8080/api/relatedness/resim'
```

Or even shorter (as `-d` or `--data` implies POST):
```
curl -D- -H 'Content-Type: application/json' -d '{"entities":[{"a":"URI1","b":"URI2"}]}' 'http://localhost:8080/api/relatedness/resim'
```



#### Response

The response is also in JSON format and follows the structure of the request.  
In addition, each "entities" entry (i.e. each entity pair) has a relatedness score `r`.  
```json
{
  "variant": "ldsd",
  "entities": [
    { "a": "firstURI1", "b": "secondURI1", "r": 0.123 },
    { "a": "firstURI2", "b": "secondURI2", "r": 0.456 }
  ]
}
```

The value of `variant` depends on the requested endpoint.
For instance, using `.../api/relatedness/ldsd` yields `ldsd`.



## Deploying a dependency to local repository

The local (internal) Maven repository of this Git repository houses dependencies that are not available in the official (remote) Maven repository.  
Therefore, we have to take care of this by including such dependencies (also called unmanaged dependencies) manually.  

More information on how to do this:  
- [Tutorial by devcenter.heroku.com](https://devcenter.heroku.com/articles/local-maven-dependencies)
- [Maven Introduction to Repositories](https://maven.apache.org/guides/introduction/introduction-to-repositories.html)

*Note*:  
You don't have to do the following to use this project!  
It is only required if you want to deploy a new dependency or version of a dependency.  

Short version using `pom` file for groupId, artifactId, version other information:
```
mvn deploy:deploy-file -Durl=file:///F:/.../semrel/repository/ -Dfile=lds/lds-0.0.2.jar -DpomFile=lds-0.0.2.pom
```

Note that we can also use `-Durl=file:///${project_loc}/repository` instead.  

A version of the command without the `pom` file:
```
mvn deploy:deploy-file -Durl=file:///F:/.../.../semrel/repository/ -Dfile=lds/lds-0.0.2.jar -DgroupId=io.github.FouadKom -DartifactId=lds -Dpackaging=jar -Dversion=0.0.2
```

Additionally, one can add the `sources.jar` and `javadoc.jar` by adding the commands:
- `-Djavadoc=lds-0.0.2-javadoc.jar`
- `-Dsources=lds-0.0.2-sources.jar`

Version without information added (can help to understand what needs to be entered):
```
mvn deploy:deploy-file -Durl=file:///<project-path>/repository/ -Dfile=<relative_path_to_jar>/<filename>.jar -DpomFile=<relative_path_to_pom>/<filename>.pom
```
