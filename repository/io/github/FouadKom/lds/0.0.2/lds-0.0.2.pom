<?xml version="1.0" encoding="UTF-8"?>
<project xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd" xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <modelVersion>4.0.0</modelVersion>
    <groupId>io.github.FouadKom</groupId>
    <artifactId>lds</artifactId>
    <version>0.0.2</version>

    <name>${project.groupId}:${project.artifactId}</name>
    <description>A Java library for Linked Open Data based semantic similarity measures.</description>
    <url>https://github.com/FouadKom/lds/blob/master/doc/General_Explanation_of_the_Library.md</url>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <jenaVersion>3.7.0</jenaVersion>
    </properties>

    <licenses>
        <license>
            <name>GNU Affero General Public License v3.0</name>
            <url>https://www.gnu.org/licenses/</url>
        </license>
    </licenses>

    <developers>
        <developer>
          <name>Fouad Komeiha</name>
          <email>fouadkomeiha.94@gmail.com</email>
          <organization>University of Tours, France</organization>
          <organizationUrl>https://www.univ-tours.fr/</organizationUrl>
        </developer>

        <developer>
          <name>Nasredine Cheniki</name>
          <email>Nacer.Cheniki@gmail.com</email>
          <organization>University of Tours, France</organization>
          <organizationUrl>https://www.univ-tours.fr/</organizationUrl>
        </developer>
    </developers>

    <dependencies>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.13.2</version>
            <type>jar</type>
        </dependency>

        <dependency>
            <groupId>org.apache.tinkerpop</groupId>
            <artifactId>gremlin-core</artifactId>
            <version>3.5.2</version>
        </dependency>

        <dependency>
            <groupId>org.apache.tinkerpop</groupId>
            <artifactId>tinkergraph-gremlin</artifactId>
            <version>3.5.2</version>
        </dependency>

        <dependency>
            <groupId>com.github.sharispe</groupId>
            <artifactId>slib-sml</artifactId>
            <version>0.9.1</version>
            <type>jar</type>
        </dependency>

        <!-- Apache Jena -->
        <dependency>
            <groupId>org.apache.jena</groupId>
            <artifactId>apache-jena-libs</artifactId>
            <version>${jenaVersion}</version>
            <type>pom</type>
        </dependency>

        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.11.0</version>
        </dependency>

        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
            <version>2.8.9</version>
        </dependency>

        <dependency>
            <groupId>org.mapdb</groupId>
            <artifactId>mapdb</artifactId>
            <version>3.0.8</version>
        </dependency>

        <dependency>
            <groupId>org.rdfhdt</groupId>
            <artifactId>hdt-api</artifactId>
            <version>2.1.2</version>
        </dependency>

        <dependency>
            <groupId>org.rdfhdt</groupId>
            <artifactId>hdt-jena</artifactId>
            <version>2.1.2</version>
            <type>jar</type>
        </dependency>

        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-math3</artifactId>
            <version>3.6.1</version>
            <type>jar</type>
        </dependency>

        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-csv</artifactId>
            <version>1.9.0</version>
            <type>jar</type>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.9.0</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
