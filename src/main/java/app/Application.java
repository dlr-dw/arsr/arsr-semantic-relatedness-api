package app;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import relatedness.MeasureManager;

/**
 * Application entry point.<br/>
 * Creates instances and starts the API server.
 */
public class Application {

	private static final Logger logger = LoggerFactory.getLogger(Application.class);
	
	private static final String APP_NAME = "Semantic Relatedness Service";
	private static final String DEFAULT_HOSTNAME = "http://localhost";
	private static final int DEFAULT_PORT = 8080;
	private static final String DEFAULT_SPARQL_ENDPOINT = "http://localhost:9999/blazegraph/sparql";
	private static final String DEFAULT_GRAPHNAME = ""; // or null
	private static final boolean DEFAULT_LDS_INDEXING = true; // use indexing feature of LDS library
	
	// used by endpoints to retrieve measure implementations
	public static MeasureManager measureManager = null;
	
	public static void main(String[] args) {
		
		// Parse properties (used with -D flag)
		// TODO: maybe provide settings in properties file
		String hostname = System.getProperty("hostname", DEFAULT_HOSTNAME);
		String port_str = System.getProperty("port", String.valueOf(DEFAULT_PORT));
		String indx_str = System.getProperty("indexing", String.valueOf(DEFAULT_LDS_INDEXING));
		boolean ldsIndexing = indx_str.equalsIgnoreCase("true");
		String sparqlEp = System.getProperty("sparqlEndpoint", DEFAULT_SPARQL_ENDPOINT);
		String defGName = System.getProperty("graph", DEFAULT_GRAPHNAME);
		
		int port = -1;
		try { port = Integer.valueOf(port_str); }
		catch (NumberFormatException ex) {
			logger.error("Failed to parse port!", ex);
			System.exit(1);
			return;
		}
		
		// log some infos about the configuration
		logger.info("LDS-Indexing {}", (ldsIndexing ? "enabled" : "disabled"));
		logger.info("SPARQL Endpoint: {}", sparqlEp);
		if (defGName != null && !defGName.isEmpty()) { logger.info("Default graph: {}", defGName); }
		
		// create measure manager (used by endpoint to run measures)
		// TODO: maybe check if sparql endpoint is even available?
		measureManager = new MeasureManager(sparqlEp, defGName, ldsIndexing);
		
		// Create server instance and let it take thread control
		API api = new API(hostname, port, APP_NAME);
		api.startServer(); // takes thread control
		
		logger.info("Exit");
		System.exit(0);
	}
}
