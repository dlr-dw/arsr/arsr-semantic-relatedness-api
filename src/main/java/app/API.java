package app;

import java.net.URI;
import org.eclipse.jetty.server.Server;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.uri.internal.JerseyUriBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import example.ExampleJSONResource;
import exceptionproviders.CustomJsonMappingExceptionHandler;
import exceptionproviders.CustomJsonParsingExceptionHandler;
import exceptionproviders.FaultyRequestExceptionHandler;
import relatedness.RelatednessResource;


public class API {
	
	private final Logger logger = LoggerFactory.getLogger(API.class);
	private Server jettyServer;
	private URI uri;
	
	public API(String hostname, int port, String app_name) {
		
		uri = JerseyUriBuilder.fromUri(hostname).port(port).build();
		
		// Create configuration and add resources (endpoints)
		ResourceConfig conf = new ResourceConfig(
			ExampleJSONResource.class,
			RelatednessResource.class
		);
		conf.property(ServerProperties.WADL_FEATURE_DISABLE, true); // disable WADL (otherwise warning in logs)
		conf.setApplicationName(app_name);
		
		// register JSON exception handlers with priority
		conf.register(CustomJsonMappingExceptionHandler.class, 1);
		conf.register(CustomJsonParsingExceptionHandler.class, 1);
		conf.register(FaultyRequestExceptionHandler.class, 1);
		
		// Using this and @Provider annotation on exception handler classes,
		// we do not need to register every single component using "register".
		// Problem: need to adjust this when changing group name or artifactId in POM.
		//conf.packages("ARSR.semrel.src.main.java.exceptionproviders");
		
		// source code of factory: https://github.com/eclipse-ee4j/jersey/blob/master/containers/jetty-http/src/main/java/org/glassfish/jersey/jetty/JettyHttpContainerFactory.java
		jettyServer = JettyHttpContainerFactory.createServer(uri, conf); // also calls server.start()
		jettyServer.setStopAtShutdown(true); // stop server with JVM shutdown
		
		// shut down server on runtime (not required because of setStopAtShutdown?)
//		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
//			
//			@Override
//			public void run() {
//				try {
//					jettyServer.stop();
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		}));
	}
	
	/**
	 * Tries to start the application server.<br/>
	 * This method will take control over the thread.<br/>
	 * Every call after this must wait until the server is stopped.
	 */
	public void startServer() {
		
		try {
			jettyServer.start();
			logger.info("Server started!");
			logger.info("API URI: {}", uri);
			jettyServer.join();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
