package exceptions;

import exceptionproviders.FaultyRequestExceptionHandler;
import jakarta.ws.rs.core.MediaType;

/**
 * This class is used to indicate that request data is missing.<br/>
 * It is handled by the custom exception provider {@link FaultyRequestExceptionHandler}.
 */
public class FaultyRequestException extends Exception {

	private static final long serialVersionUID = 1L;
	private String mediaType = null;

	public FaultyRequestException() {
		super();
	}

	public FaultyRequestException(String message, String responseType) {
		super(message);
		mediaType = responseType;
	}
	
	public FaultyRequestException(String message) {
		super(message);
	}
	
	public String getMediaType() {
		if (mediaType == null) { return MediaType.TEXT_PLAIN; }
		return mediaType;
	}

}
