package example;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Just for testing JSON binding.
 */
@XmlRootElement
public class ExampleJSONBean {

	public double relatedness[] = new double[0];
	
	public ExampleJSONBean() {}
	
	public ExampleJSONBean(double[] scores) {
		relatedness = scores;
	}
}
