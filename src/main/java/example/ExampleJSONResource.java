package example;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

/**
 * Just for testing if simple endpoints and JSON works.
 */
@Path("example")
public class ExampleJSONResource {
	
	@GET
	@Path("test")
	@Produces("text/plain")
	public String test() {
		return "Test!";
	}
	
	@GET
	@Path("json")
	@Produces(MediaType.APPLICATION_JSON)
	public ExampleJSONBean json() {
		return new ExampleJSONBean();
	}
	
	@GET
	@Path("json2")
	@Produces(MediaType.APPLICATION_JSON)
	public ExampleJSONBean json2() {
		return new ExampleJSONBean(new double[] {0.5, 0.2, 0.123});
	}
	
	/**
	 * Retrieves a JSON bean and doubles all the relatedness values.
	 * @param bean
	 */
	@POST
	@Path("json3")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ExampleJSONBean json3(ExampleJSONBean bean) {
		double relnew[] = new double[bean.relatedness.length];
		for (int i = 0; i < bean.relatedness.length; i++) {
			relnew[i] = bean.relatedness[i] * 2;
		}
		return new ExampleJSONBean(relnew);
	}
}
