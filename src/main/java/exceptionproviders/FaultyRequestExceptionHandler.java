package exceptionproviders;

import app.API;
import exceptions.FaultyRequestException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * Handles the custom exception {@link FaultyRequestException}.<br/>
 * Custom exception handlers are currently manually registered in {@link API}.
 */
@Provider
public class FaultyRequestExceptionHandler implements ExceptionMapper<FaultyRequestException> {

	@Override
	public Response toResponse(FaultyRequestException exception) {

		return Response.status(Status.BAD_REQUEST)
			.entity(exception.getMessage())
			.type(exception.getMediaType())
			.build();
	}

}
