package exceptionproviders;

import com.fasterxml.jackson.core.JsonParseException;

import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * Handle {@link JsonParseException} accordingly
 * without returning all the exception information to the client.<br/>
 * Such an exception is thrown if the client passes invalid JSON.
 */
@Provider
public class CustomJsonParsingExceptionHandler implements ExceptionMapper<JsonParseException> {

	@Override
	public Response toResponse(JsonParseException exception) {

		return Response.status(Status.BAD_REQUEST)
			.entity("Parsing JSON failed: " + exception.getOriginalMessage())
			.type(MediaType.TEXT_PLAIN)
			.build();
	}

}
