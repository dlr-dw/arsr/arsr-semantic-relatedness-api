package exceptionproviders;

import com.fasterxml.jackson.databind.JsonMappingException;

import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * Handle {@link JsonMappingException} accordingly
 * without returning all the exception information to the client.<br/>
 * Such an exception is thrown if the client passes JSON that can not be mapped to the classes.
 */
@Provider
public class CustomJsonMappingExceptionHandler implements ExceptionMapper<JsonMappingException> {

	@Override
	public Response toResponse(JsonMappingException exception) {
		
		return Response.status(Status.BAD_REQUEST)
			.entity("JSON structure not supported: " + exception.getOriginalMessage())
			.type(MediaType.TEXT_PLAIN)
			.build();
	}

}
