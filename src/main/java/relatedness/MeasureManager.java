package relatedness;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import relatedness.measures.MeasureBase;
import relatedness.measures.Measures;
import relatedness.measures.lds.LDSMeasureLDSD;
import relatedness.measures.lds.LDSMeasureResim;

/**
 * Class managing the relatedness measure implementations.<br/>
 * Kinda acts like a measure factory.
 */
public class MeasureManager {
	
	private final static Logger logger = LoggerFactory.getLogger(MeasureManager.class);
	
	private String sparqlEndpoint;
	private String defaultGraphName;
	private boolean indexing;

	/**
	 * Create an instance of the {@link MeasureManager}.
	 * @param sparqlEndpoint Address of the SPARQL endpoint to use
	 * @param defaultGraphName Default graph name (if available, otherwise set null or empty)
	 * @param indexing Enable or disable indexing support of LDS implementation
	 */
	public MeasureManager(String sparqlEndpoint, String defaultGraphName, boolean indexing) {
		this.sparqlEndpoint = sparqlEndpoint;
		this.defaultGraphName = defaultGraphName;
		this.indexing = indexing;
	}
	
	/** Create with indexing enabled by default. */
	public MeasureManager(String sparqlEndpoint, String defaultGraphName) {
		this(sparqlEndpoint, defaultGraphName, true);
	}
	
	/** Create with indexing enabled and empty graph name as default. */
	public MeasureManager(String sparqlEndpoint) {
		this(sparqlEndpoint, null, true);
	}
	
	
	/**
	 * Get an instance of a specific measure.<br/>
	 * This will also initialize its required components (like database and so on).<br/>
	 * Make sure to {@code close()} its resources once finished.
	 * @throws IllegalArgumentException if a measure is not yet available
	 */
	private MeasureBase getMeasure(Measures measure) {
		
		// We currently use the LDS implementation.
		// If we want to replace it, do it here or add additional type.
		switch(measure) {
			case LDSD_LDS:
				return new LDSMeasureLDSD(sparqlEndpoint, defaultGraphName, "ldsd", indexing);
			case Resim_LDS:
				return new LDSMeasureResim(sparqlEndpoint, defaultGraphName, "ldsd", indexing);
		}
		
		throw new IllegalArgumentException("No implementation for measure " + measure.toString());
	}
	
	
	// Following is used for now to make using measurements an sequential operation.
	// Reason: LDS uses "indexing" with "mapdb" and therefore,
	// having multiple instances writing at once could cause problems.
	
	/**
	 * Runs a measurement to acquire the relatedness of the entities.<br/>
	 * The relatedness value will be added to the according {@link EntityPair}.<br/>
	 * This method is used to prevent multiple instances to run at once.<br/>
	 * Reason is, that LDS library is probably not made for concurrency and could cause problems.<br/>
	 * Furthermore, we currently only have one request after another.
	 * @return The name of the measure executed.
	 */
	public synchronized String addRelatedness(EntityPair[] entities, Measures measure) {
		
		logger.info("Retrieving measure: " + measure.toString());
		MeasureBase mInst = getMeasure(measure);
		String mName = mInst.getName();
		logger.info("Running measure: " + mName);
		mInst.addRelatedness(entities);
		mInst.close();
		logger.info("Done.");
		return mName;
	}
}
