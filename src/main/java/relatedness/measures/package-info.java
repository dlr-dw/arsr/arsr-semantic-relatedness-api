/**
 * Provides generic interface for using different implementations.
 * For instance, we could use an own LDSD implementation or one by the LDS-library.
 */
package relatedness.measures;