package relatedness.measures.lds;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ldq.LdDataset;
import lds.config.Config;
import lds.config.ConfigParam;
import lds.dataset.LdDatasetCreator;
import lds.engine.LdSimilarityEngine;
import lds.measures.Measure;
import lds.resource.R;
import relatedness.measures.MeasureBase;

/**
 * A measure based on the implementation of LDS library.<br/>
 * The method {@link #prepareEngine()} is called on first call to a relatedness method,
 * if the engine was not previously initialized.
 */
public abstract class LDSMeasure extends MeasureBase {

	private final Logger logger = LoggerFactory.getLogger(LDSMeasure.class);
	
	protected LdDataset dataset;
	
	/** An engine is required to run the similarity function! */
	protected LdSimilarityEngine engine;
	
	protected boolean indexing = false;
	
	/**
	 * Set dataset and indexing of the measure.<br/>
	 * Does not prepare engine yet! Make sure to call {@link #prepareEngine()} afterwards.
	 * @param dataset
	 * @param indexing
	 */
	public LDSMeasure(LdDataset dataset, boolean indexing) {
		this.dataset = dataset;
		this.indexing = indexing;
	}
	
	/**
	 * Create the measure based on a sparql endpoint URI.
	 * @param sparqlEndpoint The endpoint to use.
	 * @param defaultGraph Default graph name of the data (if there is none, pass {@code null} or "").
	 * @param dsName An arbitrary name for the dataset that is created.
	 * @param indexing If indexing should be enabled.
	 */
	public LDSMeasure(String sparqlEndpoint, String defaultGraph, String dsName, boolean indexing) {
		this(prepareRemoteDataset(sparqlEndpoint, defaultGraph, dsName), indexing);
	}
	
	/**
	 * Creates and configures an {@link LdSimilarityEngine}.<br/>
	 * Also calls {@link #extendConfig(Config)} in the process.
	 */
	public void prepareEngine() {
		
		// basic configuration for dataset and indexing
		Config conf = new Config();
		conf.addParam(ConfigParam.LdDatasetMain, dataset);
		conf.addParam(ConfigParam.useIndexes, indexing);
		extendConfig(conf);
		
		// the actual engine
		this.engine = new LdSimilarityEngine();
		engine.load(getMeasure(), conf);
	}
	
	/**
	 * Returns the type of measure used.<br/>
	 * Must return an actual valid LDS measure to work!
	 */
	public abstract Measure getMeasure();
	
	/**
	 * Use this method to extend the basic configuration,
	 * which already contains the dataset to use
	 * and the indexing parameter set accordingly.<br/>
	 * Extension of this config can be optional so thatthis method can be empty.<br/>
	 * For instance, PICSS requires an additional parameter (resourceCount).<br/>
	 * Use this method to add this parameter to the configuration.
	 * @param config Already initialized configuration with dataset and indexing set
	 */
	public abstract void extendConfig(Config config);
	
	/**
	 * Computes relatedness of two entities.<br/>
	 * Note that these need to be resource URIs for LDS to work!
	 * @param entity1 URI of first resource
	 * @param entity2 URI of second resource
	 * @throws RuntimeException if similarity engine was not initialized yet
	 */
	@Override
	public double relatedness(String entity1, String entity2) {
		
		// initialize the engine if not done yet
		if (engine == null) { prepareEngine(); }
		
		R r1 = new R(entity1);
		R r2 = new R(entity2);
		return relatedness(r1, r2);
	}
	
	/**
	 * Computes relatedness of the entities using the similarity engine.
	 * @param entity1
	 * @param entity2
	 */
	public double relatedness(R entity1, R entity2) {
		
		// initialize the engine if not done yet
		if (engine == null) { prepareEngine(); }
		return engine.similarity(entity1, entity2);
	}
	
	/**
	 * Close the used dataset and engine resources.<br/>
	 * Make sure to call this method once you are finished using the measure.<br/>
	 * After calling it, do no longer use methods of this instance.
	 */
	public void close() {
		try {
			if (dataset != null) { dataset.close(); }
			if (engine != null) { engine.close(); }
			logger.debug("Resources closed for measure " + getName());
		}
		catch (Exception ex) {
			logger.error("Failed to close measure resources (" + getName() + ")!", ex);
		}
	}

	/**
	 * Create an {@link LdDataset} based on a sparql endpoint.
	 * @param sparqlEndpoint The address of the endpoint to use.
	 * @param defaultGraph Name of the default graph to use (set {@code null} or "" if there is no specific).
	 * @param name An arbitrary name for the dataset (not important).
	 * @return
	 */
	public static LdDataset prepareRemoteDataset(String sparqlEndpoint, String defaultGraph, String name) {
		return LdDatasetCreator.getRemoteDataset(sparqlEndpoint, null, name);
	}
}
