package relatedness.measures.lds;

import ldq.LdDataset;
import lds.config.Config;
import lds.measures.Measure;

/**
 * Linked Data Semantic Similarity (LDSD) measure implementation by LDS library.
 */
public class LDSMeasureLDSD extends LDSMeasure {

	public LDSMeasureLDSD(LdDataset dataset, boolean indexing) {
		super(dataset, indexing);
	}

	/**
	 * Create based on sparql endpoint URI.<br/>
	 * For more see {@link LDSMeasure#LDSMeasure(String, String, String, boolean)}.
	 */
	public LDSMeasureLDSD(String sparqlEndpoint, String defaultGraph, String dsName, boolean indexing) {
		super(sparqlEndpoint, defaultGraph, dsName, indexing);
	}
	
	@Override
	public Measure getMeasure() {
		return Measure.LDSD_cw;
	}

	@Override
	public void extendConfig(Config config) {
		// nothing to extend
	}

	@Override
	public String getName() {
		return "LDSD";
	}
}
