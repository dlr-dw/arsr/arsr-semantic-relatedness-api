package relatedness.measures;

/**
 * List of currently available measures.
 */
public enum Measures {

	/** LDS implementation of LDSD */
	LDSD_LDS,
	/** LDS implementation of Resim */
	Resim_LDS
}
