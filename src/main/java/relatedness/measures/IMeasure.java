package relatedness.measures;

/**
 * Interface that various measures must implement.
 */
public interface IMeasure {
	
	public double relatedness(String entity1, String entity2);
	
	/** Returns the name of this measure. */
	public String getName();
	
	/**
	 * Close resources once finished using the measure.<br/>
	 * Don't use the measure after closing!
	 */
	public void close();
}
