package relatedness.measures;

import relatedness.EntityPair;

/**
 * Base class for measures.<br/>
 * Provides a method to add relatedness to a set of {@link EntityPair} instances.<br/>
 * The method sets 0 as relatedness if one of the entities of a pair is empty.
 */
public abstract class MeasureBase implements IMeasure {
	
	public void addRelatedness(EntityPair[] entities) {
		
		for (EntityPair p : entities) {
			
			// TODO: Note that computation can crash if connection to the endpoint fails (need to fix this in LDS-library)
			if (p.first.isEmpty() || p.second.isEmpty()) { p.relatedness = 0; }
			else { p.relatedness = relatedness(p.first, p.second); }
		}
	}
}
