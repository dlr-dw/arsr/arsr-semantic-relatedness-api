package relatedness;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class that represents a pair of entities
 * for which relatedness should be computed.
 */
public class EntityPair {

	@JsonProperty("a")
	public String first = "";
	
	@JsonProperty("b")
	public String second = "";
	
	// Not that this can already be set by the client as well
	// and we could further use this information if required.
	@JsonProperty("r")
	public double relatedness = 0;
}
