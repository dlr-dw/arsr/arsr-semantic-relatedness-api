package relatedness;

import app.Application;
import exceptions.FaultyRequestException;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import relatedness.measures.Measures;

/**
 * Endpoint resource for relatedness computation.
 */
@Path("api/relatedness")
public class RelatednessResource {

	/**
	 * Raises a {@link FaultyRequestException} if data is null.
	 * @throws FaultyRequestException
	 */
	private void checkNull(Object data, String message) throws FaultyRequestException {
		if (data == null) { throw new FaultyRequestException(message); }
	}

	@POST
	@Path("ldsd")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public RelatednessResult ldsd(RelatednessRequest data) throws FaultyRequestException {
		checkNull(data, "Missing Json data!");
		String measureName = Application.measureManager.addRelatedness(data.entities, Measures.LDSD_LDS);
		return new RelatednessResult(data.entities, measureName);
	}

	@POST
	@Path("resim")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public RelatednessResult resim(RelatednessRequest data) throws FaultyRequestException {
		checkNull(data, "Missing Json data!");
		String measureName = Application.measureManager.addRelatedness(data.entities, Measures.Resim_LDS);
		return new RelatednessResult(data.entities, measureName);
	}
}
