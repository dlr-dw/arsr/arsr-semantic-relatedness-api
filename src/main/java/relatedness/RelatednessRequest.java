package relatedness;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * User request data holding entities to compute relatedness for.
 */
@XmlRootElement
public class RelatednessRequest {

	public EntityPair entities[] = new EntityPair[0];
}
