package relatedness;

/**
 * Result class that holds response information.
 */
public class RelatednessResult extends RelatednessRequest {

	public String variant = ""; // e.g. ldsd, resim, ...
	
	public RelatednessResult() {}
	
	public RelatednessResult(EntityPair[] entities, String variant) {
		this.entities = entities;
		this.variant = variant;
	}
}
